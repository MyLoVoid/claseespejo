# *Jupyter notebooks* para la clase espejo

Para inciar una maquina virtual (reciclable) y ejecutar los jupyter notebooks oprime el siguiente enlace
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/MyLoVoid%2Fclaseespejo/master)

**Advertencia:** el lanzamiento inicial del recurso en Binder puede demorar un par de minutos mientras se generan o cargan contenedores o máquinas virtuales en la nube de Binder. Tener paciencia al respecto.
